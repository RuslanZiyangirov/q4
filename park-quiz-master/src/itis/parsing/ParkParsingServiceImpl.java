package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.Scanner;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        //write your code here

        Class<?> clazz = null;
        Park park = null;
        try {
            clazz = Class.forName("itis.parsing.Park");
            park = (Park) clazz.getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        try {
            FileReader fileReader = new FileReader(parkDatafilePath);
            Scanner scanner = new Scanner(fileReader);
            while (scanner.hasNextLine()){
                String lineOfText = scanner.nextLine();

                if (lineOfText.equals("***")){
                    continue;
                }

                String[] columns = lineOfText.split(":");


                if (columns[0].charAt(0)=='"'){
                    columns[0]= columns[0].substring(1,columns[0].length()-1);
                }
                if (columns[1].charAt(0)==' '){
                    columns[1] = columns[1].substring(2, columns[1].length()-1);
                }else{
                    columns[1] = columns[1].substring(1, columns[1].length()-1);
                }


                Field[] fields = clazz.getDeclaredFields();
                for (Field field:fields){
                    FieldName fieldName = field.getAnnotation(FieldName.class);
                    MaxLength maxLength = field.getAnnotation(MaxLength.class);
                    NotBlank notBlank = field.getAnnotation(NotBlank.class);

                    if(fieldName != null){
                        if (columns[0].equals(fieldName.value())){
                            field.setAccessible(true);
                            try {
                                field.set(park,columns[1]);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if(maxLength != null && notBlank != null){
                        if (columns[1].length()<= maxLength.value() && columns[0].equals("ownerOrganizationInn")){
                            field.setAccessible(true);
                            try {
                                field.set(park,columns[1]);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (notBlank != null){
                        if (columns[0].equals("foundationYear")){
                            String[] dataArray = columns[1].split("-");
                            int years = Integer.parseInt(dataArray[0]);
                            int month = Integer.parseInt(dataArray[1]);
                            int day = Integer.parseInt(dataArray[2]);
                            LocalDate date = LocalDate.of(years,month,day);
                            field.setAccessible(true);
                            try {
                                field.set(park,date);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }


            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return null;
    }
}
